import ro.ubb.bookstore.UI.UI;
import ro.ubb.bookstore.controller.Controller;
import ro.ubb.bookstore.model.Book;
import ro.ubb.bookstore.model.Client;
import ro.ubb.bookstore.model.validators.BookValidator;
import ro.ubb.bookstore.model.validators.ClientValidator;
import ro.ubb.bookstore.repository.*;

/**
 * Created by Dani on 3/13/2016.
 */
public class Main {

    /**
     * Main function of the program.This is the function called when the program starts.
     * @param args
     */
    public static void main(String args[]) {
        BookRepo bookRepo = new BookRepo();
        XMLBookRepo xmlBookRepo = new XMLBookRepo();
        XMLClientRepo xmlClientRepo = new XMLClientRepo();


        String url = "jdbc:postgresql://localhost:5432/bookstore";
        String admin = "postgres";
        String pass = "admin";

        BookDbRepo bookDbRepo = new BookDbRepo(url,admin,pass);
        ClientDbRepo clientDbRepo = new ClientDbRepo(url,admin,pass);

        ClientRepo clientRepo = new ClientRepo();
        clientRepo.add_client(new Client(1,"a","b",20,15));
        bookRepo.add_book(new Book(1,"Titlu1","Author1","Genre1",1,10,5));
        bookRepo.add_book(new Book(2,"Titlu2","Author2","Genre2",2,20,10));
        bookRepo.add_book(new Book(3,"Titlu3","Author3","Genre3",3,30,15));

        BookValidator bookValidator = new BookValidator();
        ClientValidator clientValidator = new ClientValidator();

        Controller controller = new Controller(bookValidator, bookRepo, xmlBookRepo , xmlClientRepo, clientDbRepo, bookDbRepo, clientValidator, clientRepo);
        UI ui = new UI(controller);
        ui.run();
    }
}

