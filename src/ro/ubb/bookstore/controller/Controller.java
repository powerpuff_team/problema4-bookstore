package ro.ubb.bookstore.controller;

import org.xml.sax.SAXException;
import ro.ubb.bookstore.model.Book;
import ro.ubb.bookstore.model.Client;
import ro.ubb.bookstore.model.validators.BookValidator;
import ro.ubb.bookstore.model.validators.ClientValidator;
import ro.ubb.bookstore.repository.*;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Mapnah on 3/8/2016.
 */
public class Controller {
    private BookValidator book_val;
    private BookRepo book_repo;
    private XMLBookRepo xmlBookRepo;
    private XMLClientRepo xmlClientRepo;
    private ClientValidator client_val;
    private ClientRepo client_repo;
    private ClientDbRepo clientDbRepo;
    private BookDbRepo bookDbRepo;

    public Controller(BookValidator bookValidator,BookRepo book_repo, XMLBookRepo xmlBookRepo, XMLClientRepo xmlClientRepo,ClientDbRepo clientDbRepo, BookDbRepo bookDbRepo, ClientValidator clientValidator,ClientRepo client_repo){
        this.book_val = bookValidator;
        this.book_repo = book_repo;
        this.xmlBookRepo = xmlBookRepo;
        this.client_val = clientValidator;
        this.client_repo = client_repo;
        this.xmlClientRepo = xmlClientRepo;
        this.clientDbRepo = clientDbRepo;
        this.bookDbRepo = bookDbRepo;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //BOOK OPERATIONS

    //BOOK DATABASE OPERATIONS
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    /**
     * This function returns from the database the book whose id is equal to id
     * @param id
     * @return
     */
    public Optional<Book> getBookByIDFromDB(Long id){
       return this.bookDbRepo.findOne(id);
    }

    /**
     * This function returns all books from the database.
     * @return
     */
    public List<Book> getBooksFromDB(){
        return this.bookDbRepo.findAll();
    }

    public void saveBooksToDB(){
        List<Book> books = get_books();
        try {
            this.bookDbRepo.saveAll(books);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    /**
     * Creates a book object and tries to add it to the database by calling add function from book repo.
     * @param id
     * @param title
     * @param author
     * @param genre
     * @param review
     * @param price
     * @param noOfCopies
     */
    public void add_book(int id,String title,String author,String genre,int review,float price,int noOfCopies) {
        Book book = new Book(id, title, author, genre, review, price, noOfCopies);
        String errors = book_val.validate(book);
        if (errors == "") {
            book_repo.add_book(book);
        } else {
            System.out.println(errors);
        }
    }

    /**
     * Creates a book object and tries to update it by calling update function from book repo.
     * @param id
     * @param title
     * @param author
     * @param genre
     * @param review
     * @param price
     * @param noOfCopies
     */
    public void update_book(int id,String title,String author,String genre,int review,float price,int noOfCopies){
        Book book = new Book(id,title,author,genre,review,price,noOfCopies);
        String errors=book_val.validate(book);
        if(errors == ""){
            book_repo.update_book(book);
        }
        else{
            System.out.println(errors);
        }
    }

    /**
     * Tries to remove the book with the given ID by calling the remove function from book repo.
     * @param id
     */
    public void remove_book(int id){
        book_repo.remove_book(id);
    }

    /**
     *
     * @return A list of all the books in the database.
     */
    public List<Book> get_books(){
        return book_repo.getBooks();
    }

    /**
     *
     * @return Size of the list of books.
     */
    public int get_bookDB_size(){
        return book_repo.getSize();
    }





    public void book_load_XML() throws IOException, SAXException, ParserConfigurationException {
        this.book_repo.clear();

            for (Book book:xmlBookRepo.loadBooks()) {
                if(book_repo.search_by_ID(book.get_ID()) == null)
                    this.book_repo.add_book(book);
            }

        }


    public void book_save_XML(){
        try {
            xmlBookRepo.saveBooks(book_repo.getBooks());
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }



    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //CLIENT OPERATIONS

    /**
     * Creates a client object and tries to add it to the database by calling the add function in the client repo.
     * @param id
     * @param first_name
     * @param last_name
     * @param age
     * @param money_spent
     */

    public void add_client(int id,String first_name,String last_name,int age,float money_spent) {
        Client client = new Client(id, first_name, last_name, age, money_spent);
        String errors = client_val.validate(client);
        if (errors == "") {
            client_repo.add_client(client);
        } else {
            System.out.println(errors);
        }
    }
    /**
     * Creates a client object and tries to update it by calling the update function in the client function.
     * @param id
     * @param first_name
     * @param last_name
     * @param age
     * @param money_spent
     */

    public void update_client(int id,String first_name,String last_name,int age,float money_spent){
        Client client = new Client(id,first_name,last_name,age,money_spent);
        client_repo.update_client(client);
    }

    /**
     * Tries to remove the client with the given ID by calling the remove function from the client repo.
     * @param id
     */
    public void remove_client(int id){
        client_repo.remove_client(id);
    }

    /**
     *
     * @return A list of all the clients in the database.
     */
    public List<Client> get_clients(){
        return client_repo.getClients();
    }

    /**
     *
     * @return The size of the list of clients.
     */
    public int get_clientDB_size(){
        return client_repo.getSize();
    }

    public void client_load_XML() throws IOException, SAXException, ParserConfigurationException {
        this.client_repo.clear();

        for (Client client:xmlClientRepo.loadClients()) {
            if(client_repo.search_by_ID(client.get_ID()) == null)
                this.client_repo.add_client(client);
        }

    }


    public void client_save_XML(){
        try {
            xmlClientRepo.saveClients(client_repo.getClients());
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }

    //CLIENT DATABASE OPERATIONS
    /**
     * This function returns from the database the book whose id is equal to id
     * @param id
     * @return
     */
    public Optional<Client> getClientByIDFromDB(Long id){
        return this.clientDbRepo.findOne(id);
    }

    /**
     * This function returns all books from the database.
     * @return
     */
    public List<Client> getClientsFromDB(){
        return this.clientDbRepo.findAll();
    }

    public void saveClientsToDB(){
        List<Client> clients = get_clients();
        try {
            this.clientDbRepo.saveAll(clients);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //FILTER OPERATIONS

    /**
     * Filters books by title.
     * @param title
     * @return A new list with only the books having a certain title.
     */

    public List<Book> book_filter_by_title(String title){
        List<Book> bookList = book_repo.getBooks();
        List<Book> result = bookList.stream().filter(book -> book.get_title().equals(title)).collect(Collectors.toList());
        return result;
    }

    /**
     * Filters books by author.
     * @param author
     * @return A new list with only the books having a certain author.
     */
    public List<Book> book_filter_by_author(String author){
        List<Book> bookList = book_repo.getBooks();
        List<Book> result = bookList.stream().filter(book -> book.get_author().equals(author)).collect(Collectors.toList());
        return result;
    }


    /**
     * Filters books by genre.
     * @param genre
     * @return A new list with only the books having a certain genre.
     */
    public List<Book> book_filter_by_genre(String genre){
        List<Book> bookList = book_repo.getBooks();
        List<Book> result = bookList.stream().filter(book -> book.get_genre().equals(genre)).collect(Collectors.toList());
        return result;
    }

    /**
     * Filters books by review.
     * @param review
     * @return A new list with only the books having a certain review.
     */

    public List<Book> book_filter_by_review(int review){

        List<Book> bookList = book_repo.getBooks();
        List<Book> result = bookList.stream().filter(book -> book.get_review()==review).collect(Collectors.toList());
        return result;
    }

    /**
     * Filters clients by last name.
     * @param last_name
     * @return A new list with only the clients having a certain last name.
     */
    public List<Client> client_filter_by_last_name(String last_name){
        List<Client> clientList = client_repo.getClients();
        List<Client> result = clientList.stream().filter(client -> client.get_last_name().equals(last_name)).collect(Collectors.toList());
        return result;
    }

    /**
     * Filters clients by first name.
     * @param first_name
     * @return A new list with only the clients having a certain first name.
     */
    public List<Client> client_filter_by_first_name(String first_name){
        List<Client> clientList = client_repo.getClients();
        List<Client> result = clientList.stream().filter(client -> client.get_first_name().equals(first_name)).collect(Collectors.toList());
        return result;
    }

    /**
     * Filters clients by age.
     * @param age
     * @return A new list with only the clients having a certain age.
     */
    public List<Client> client_filter_by_age(int age){

        List<Client> clientList = client_repo.getClients();
        List<Client> result = clientList.stream().filter(client -> client.get_age()<=age).collect(Collectors.toList());
        return result;
    }

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//SORT OPERATIONS

    public List<Client> client_sort_by_age(){
        List<Client> clientList =  client_repo.getClients();

        List<Client> result = clientList.stream()
                .sorted(
                        (client1, client2) -> Integer.compare(client1.get_age(), client2.get_age())
                ).collect(Collectors.toList());

        return result;
    }

    public List<Book> book_sort_by_price(){
        List<Book> bookList = book_repo.getBooks();

        List<Book> result = bookList.stream()
                .sorted(
                        (book1, book2) -> Float.compare(book1.get_price(), book2.get_price())
                ).collect(Collectors.toList());

        return result;
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //BUY/RESTOCK OPERATIONS

    /**
     * The process of a client buying a book(with all the necessary checks => whether there are enough copies in the store,or if the book or the client exists at all.
     * @param client_id
     * @param book_id
     * @param copiesToBeBought
     */
    public void client_buys_book(int client_id,int book_id,int copiesToBeBought){
        Book book = book_repo.search_by_ID(book_id);
        Client client = client_repo.search_by_ID(client_id);
        if(book == null){
            System.out.println("Book was not found in Book Database!");
        }
        else{
            if(client == null){
                System.out.println("Client was not found in Client Database!");
            }
            else {
                //If program reaches this point, it means that both the book and the client exist and were found in their databasses
                if (book.get_noOfCopies() < copiesToBeBought) {
                    System.out.println("There are not enought available copies of that book!");
                } else {
                    client.set_money_spent(client.get_money_spent() + copiesToBeBought*book.get_price());
                    book.set_noOfCopies(book.get_noOfCopies() - copiesToBeBought);
                    System.out.println("Client " + client.get_last_name() + " " + client.get_first_name() + " bought " + copiesToBeBought + " copy(s) of the book " + "\"" + book.get_title() + "\".");
                }
            }
        }
    }


    /**
     * The process of restocking a certain book.
     * @param id
     * @param newNoOfCopies
     */
    public void book_restock(int id,int newNoOfCopies){
        Book book = book_repo.search_by_ID(id);
        if (book == null){
            System.out.println("Book was not found in Book Database!");
        }else{
            //Book exists and was found in the database.
            book.set_noOfCopies(book.get_noOfCopies() + newNoOfCopies);
            System.out.println("New copes were successfully added.");
        }
    }
}
