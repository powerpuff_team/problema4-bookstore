package ro.ubb.bookstore.repository;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import ro.ubb.bookstore.model.Book;
import ro.ubb.bookstore.util.UtilFunctions;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



/**
 * Created by Administrator on 3/21/2016.
 */
public class XMLBookRepo extends BookRepo {
    private String fileName;

    public static List<Book> loadBooks() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse("books.xml");



        Node root = document.getDocumentElement();
        NodeList nodeList = root.getChildNodes();
        List<Book> books = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node instanceof Element) {
                Element element = (Element) node;
                Book book = createBook(element);
                books.add(book);
            }
        }
        return books;
    }





    private static Book createBook(Element bookNode) {



        String id = getNodeValue(bookNode,"id");
        String title = getNodeValue(bookNode, "title");
        String author = getNodeValue(bookNode, "author");
        String genre = getNodeValue(bookNode, "genre");
        String review = getNodeValue(bookNode,"review");
        String price = getNodeValue(bookNode, "price");
        String nrOfCopies = getNodeValue(bookNode, "noOfCopies");

        Book book = new Book(Integer.parseInt(id),title,author,genre,UtilFunctions.starsToNumber(review),Float.parseFloat(price),Integer.parseInt(nrOfCopies));

        return book;
    }

    private static String getNodeValue(Element bookNode, String nodeName) {
        NodeList nodeList = bookNode.getElementsByTagName(nodeName);
        Node node = nodeList.item(0);
        return node.getTextContent();
    }

    public static void saveBooks(List<Book> books)
            throws ParserConfigurationException, IOException, SAXException, TransformerException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse("books.xml");
        Node root = document.getDocumentElement();

        for (Book book : books) {

            Node bookNode = document.createElement("book");
            root.appendChild(bookNode);


            appendChildElement(document, bookNode, "id", String.valueOf(book.get_ID()));
            appendChildElement(document, bookNode, "title", book.get_title());
            appendChildElement(document, bookNode, "author", book.get_author());
            appendChildElement(document, bookNode, "genre", book.get_genre());
            appendChildElement(document, bookNode, "review", String.valueOf(book.get_review()));
            appendChildElement(document, bookNode, "price", String.valueOf(book.get_price()));
            appendChildElement(document, bookNode, "noOfCopies", String.valueOf(book.get_noOfCopies()));


            Transformer transformerFactory = TransformerFactory.newInstance().newTransformer();
            DOMSource domSource = new DOMSource(document);
            Result result = new StreamResult(new File("books.xml"));

            transformerFactory.setOutputProperty(OutputKeys.INDENT, "yes");

            transformerFactory.transform(domSource, result);
        }
    }


    private static void appendChildElement(Document document, Node bookNode, String tagName, String textContent) {
        Node titleNode = document.createElement(tagName);
        titleNode.setTextContent(textContent);
        bookNode.appendChild(titleNode);
    }
}



