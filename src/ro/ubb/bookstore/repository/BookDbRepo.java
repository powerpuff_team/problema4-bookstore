package ro.ubb.bookstore.repository;

import ro.ubb.bookstore.model.Book;
import sun.security.validator.ValidatorException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;



/**
 * Created by Administrator on 3/31/2016.
 */
public class BookDbRepo extends BookRepo {
    private String url;
    private String username;
    private String password;


    public BookDbRepo(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public Optional<Book> findOne(Long id){
        if (id == null){
            throw new IllegalArgumentException("Id must not be null!");
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from book where id=?")) {
            System.out.println("Connection succesful!");
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    int bookId = resultSet.getInt("id");
                    String title = resultSet.getString("title");
                    String genre = resultSet.getString("genre");
                    String author = resultSet.getString("author");
                    int review = resultSet.getInt("review");
                    float price = resultSet.getFloat("price");
                    int nrOfCopies = resultSet.getInt("nrOfCopies");

                    Book book = new Book(bookId,title,author,genre,review,price,nrOfCopies);
                    return Optional.of(book);



                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }


    public List<Book> findAll() {
        List<Book> bookList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from book");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                int bookId = resultSet.getInt("id");
                String title = resultSet.getString("title");
                String genre = resultSet.getString("genre");
                String author = resultSet.getString("author");
                int review = resultSet.getInt("review");
                float price = resultSet.getFloat("price");
                int nrOfCopies = resultSet.getInt("nrOfCopies");
                Book book = new Book(bookId,title,author,genre,review,price,nrOfCopies);
                bookList.add(book);
            }
            return bookList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bookList;
    }


    private List<Integer> findIDs() {
        List<Integer> IDList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from book");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                IDList.add(resultSet.getInt("id"));

            }
            return IDList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return IDList;
    }


    public void saveAll(List<Book> books) throws ValidatorException {
        if (books == null) {
            throw new IllegalArgumentException("You must have something in memory");
        }

        List<Integer> IDList = findIDs();
        try (Connection connection = DriverManager.getConnection(url, username, password)){

            for (Book book : books){


                // Pass any books that are already in the DB
                if (IDList.contains(book.get_ID())){
                    System.out.println("ID already exists.");
                    System.out.println("Skipping book "+book);
                    continue;
                }

             PreparedStatement statement = connection
                     .prepareStatement("INSERT into book (id,title,author,genre,review,nrofcopies,price) VALUES (?,?,?,?,?,?,?)");{

                statement.setInt(1, book.get_ID());
                statement.setString(2, book.get_title());
                statement.setString(3, book.get_author());
                statement.setString(4, book.get_genre());
                statement.setInt(5, book.get_review());
                statement.setInt(6, book.get_noOfCopies());
                statement.setFloat(7, book.get_price());

                    statement.executeUpdate();

                    System.out.println("Book "+book+" added to DB succesfully.");
            }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /*
    @Override
    public Optional<Student> delete(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("id must not be null");
        }
        Optional<Student> student = findOne(id); // :(

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM student where id=?")) {
            statement.setLong(1, id);

            statement.executeUpdate();

            return Optional.empty();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return student;
    }

    @Override
    public Optional<Student> update(Student entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("entity must not be null");
        }
        validator.validate(entity);

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection
                     .prepareStatement("UPDATE student SET serial_number=?,name=?,group_number=? where id=?")) {
            statement.setString(1, entity.getSerialNumber());
            statement.setString(2, entity.getName());
            statement.setInt(3, entity.getGroupNumber());
            statement.setLong(4, entity.getId());

            statement.executeUpdate();

            return Optional.empty();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.of(entity);
    }
}
*/



}
