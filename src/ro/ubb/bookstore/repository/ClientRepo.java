package ro.ubb.bookstore.repository;

import ro.ubb.bookstore.model.Client;
import ro.ubb.bookstore.model.validators.ClientValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mapnah on 3/7/2016.
 */
public class ClientRepo {
    //private Client[] cdb;
    private List<Client> clientList;

    public ClientRepo(){
        clientList = new ArrayList<>();
    }

    public int getSize(){
        return clientList.size();
    }

    /**
     * Tries to add a client in the database.
     * @param client
     * @return A string showing the result of the try of adding the client in the database.
     */
    public void add_client(Client client) {
        //aici se face validare; rezolvati maine. tre sa aveti un validator cam asa ceva,
        // da` trebe facut dep injection pe el. Also, tre sa respectati structura de pe grup.
        ClientValidator clientValidator = new ClientValidator();
        clientValidator.validate(client);
        clientList.add(client);
    }

    /**
     * Tries to update a client from the database.
     * @param client
     * @return A string showing the result of the try of updating the client in the database.
     */
    public void update_client(Client client){
        int counter = 0;
        for(Client c: clientList){
            if(c.get_ID()==client.get_ID()) break;
            counter++;
        }
        clientList.set(counter,client);
    }

    /**
     * Tries to remove a client from the database.
     * @param ID
     * @return A string showing the result of the try of removing a client from the database.
     */
    public void remove_client(int ID){
        int counter = 0;
        for(Client c: clientList){
            if(c.get_ID()==ID) break;
            counter++;
        }
        clientList.remove(counter);
    }

    /**
     *
     * @return A list with the clients in the database.
     */
    public List<Client> getClients(){
        return clientList;
    }

    /**
     * Searches the database for a client,by id.
     * @param id
     * @return The client,if it is found,or null otherwise.
     */
    public Client search_by_ID(int id){
        int counter = 0;
        for(Client c: clientList){
            if(c.get_ID()==id) return c;
            counter++;
        }
        return null;
    }

    public void clear(){
        this.clientList.clear();
    }
}
