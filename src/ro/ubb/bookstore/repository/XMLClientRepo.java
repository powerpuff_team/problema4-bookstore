package ro.ubb.bookstore.repository;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import ro.ubb.bookstore.model.Client;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dani on 3/28/2016.
 */
public class XMLClientRepo {

    private String fileName;

    public static List<Client> loadClients() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse("clients.xml");


        Node root = document.getDocumentElement();
        NodeList nodeList = root.getChildNodes();
        List<Client> clients = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node instanceof Element) {
                Element element = (Element) node;
                Client client = createClient(element);
                clients.add(client);
            }
        }
        return clients;
    }





    private static Client createClient(Element bookNode) {



        String id = getNodeValue(bookNode,"id");
        String first_name = getNodeValue(bookNode, "first_name");
        String last_name = getNodeValue(bookNode, "last_name");
        String age = getNodeValue(bookNode, "age");
        String money_spent = getNodeValue(bookNode,"money_spent");

        Client client = new Client(Integer.parseInt(id),first_name,last_name,Integer.parseInt(age),Float.parseFloat(money_spent));

        return client;
    }

    private static String getNodeValue(Element bookNode, String nodeName) {
        NodeList nodeList = bookNode.getElementsByTagName(nodeName);
        Node node = nodeList.item(0);
        return node.getTextContent();
    }

    public static void saveClients(List<Client> clients)
            throws ParserConfigurationException, IOException, SAXException, TransformerException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse("clients.xml");
        Node root = document.getDocumentElement();

        /*if(root.hasChildNodes()) {
            for (int i = 0; i < root.getChildNodes().getLength(); i++) {
                Node node = root.getChildNodes().item(i);
                root.removeChild(node);
            }
        }*/

        for (Client client: clients) {

            Node clientNode = document.createElement("client");
            root.appendChild(clientNode);



            appendChildElement(document, clientNode, "id", String.valueOf(client.get_ID()));
            appendChildElement(document, clientNode, "first_name", client.get_first_name());
            appendChildElement(document, clientNode, "last_name", client.get_last_name());
            appendChildElement(document, clientNode, "age", String.valueOf(client.get_age()));
            appendChildElement(document, clientNode, "money_spent", String.valueOf(client.get_money_spent()));


            Transformer transformerFactory = TransformerFactory.newInstance().newTransformer();
            DOMSource domSource = new DOMSource(document);
            Result result = new StreamResult(new File("clients.xml"));

            transformerFactory.setOutputProperty(OutputKeys.INDENT, "yes");

            transformerFactory.transform(domSource, result);
        }
    }


    private static void appendChildElement(Document document, Node bookNode, String tagName, String textContent) {
        Node titleNode = document.createElement(tagName);
        titleNode.setTextContent(textContent);
        bookNode.appendChild(titleNode);
    }
}
