package ro.ubb.bookstore.repository;

import ro.ubb.bookstore.model.Book;
import ro.ubb.bookstore.model.validators.BookValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mapnah on 3/7/2016.
 */
public class BookRepo {

    //private Book[] bdb;
    private List<Book> bookList;

    public BookRepo(){
        bookList = new ArrayList<>();
    }

    public int getSize(){
        return bookList.size();
    }

    /**
     * Tries to add the book to the database.
     * @param book
     * @return A string showing the result of the try of adding the book in the database.
     */
    public void add_book(Book book) {
        BookValidator bookValidator = new BookValidator();
        bookValidator.validate(book);
        bookList.add(book);
    }

    /**
     * Tries to update the book in the database.
     * @param book
     * @return A string showing the result of the try of updating the book in the database.
     */
    public void update_book(Book book){
        int counter = 0;
        for (Book b : bookList){
            if(b.get_ID()==book.get_ID()) break;
            counter++;
        }
        bookList.set(counter,book);
    }

    /**
     * Tries to remove a book from the database.
     * @param ID
     * @return A string showing the result of the try of adding the book in the database.
     */
    public void remove_book(int ID){
        int counter = 0;
        for(Book b: bookList){
            if(b.get_ID()==ID) break;
            counter++;
        }
        bookList.remove(counter);
    }

    /**
     *
     * @return A list with the books in the database.
     */
    public List<Book> getBooks(){
        return bookList;
    }

    /**
     * Searches the database for a book by id.
     * @param id
     * @return The book,if it is found,or null otherwise.
     */
    public Book search_by_ID(int id){
        int counter = 0;
        for(Book b: bookList){
            if(b.get_ID()==id) return b;
            counter++;
        }
        return null;
    }






    public void clear(){
        this.bookList.clear();
    }
}
