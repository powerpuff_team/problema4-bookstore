package ro.ubb.bookstore.repository;

import ro.ubb.bookstore.model.Client;
import sun.security.validator.ValidatorException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Administrator on 3/31/2016.
 */
public class ClientDbRepo {
    private String url;
    private String username;
    private String password;


    public ClientDbRepo(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public Optional<Client> findOne(Long id){
        if (id == null){
            throw new IllegalArgumentException("Id must not be null!");
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from client where id=?")) {
            System.out.println("Connection succesful!");
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    int clientId = resultSet.getInt("id");
                    String first_name = resultSet.getString("firstname");
                    String last_name = resultSet.getString("lastname");
                    int age = resultSet.getInt("age");
                    float money_spent = resultSet.getFloat("moneyspent");
                    Client client = new Client(clientId,first_name,last_name,age,money_spent);
                    return Optional.of(client);



                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }


    public List<Client> findAll() {
        List<Client> clientList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from client");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                int clientId = resultSet.getInt("id");
                String first_name = resultSet.getString("firstname");
                String last_name = resultSet.getString("lastname");
                int age = resultSet.getInt("age");
                float money_spent = resultSet.getFloat("moneyspent");
                Client client = new Client(clientId,first_name,last_name,age,money_spent);
                clientList.add(client);
            }
            return clientList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clientList;
    }



    private List<Integer> findIDs() {
        List<Integer> IDList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from client");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                IDList.add(resultSet.getInt("id"));

            }
            return IDList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return IDList;
    }

    public void saveAll(List<Client> clients) throws ValidatorException {
        if (clients == null) {
            throw new IllegalArgumentException("You must have something in memory");
        }

        List<Integer> IDList = findIDs();
        try (Connection connection = DriverManager.getConnection(url, username, password)){

            for (Client client : clients){


                // Pass any clients that are already in the DB
                if (IDList.contains(client.get_ID())){
                    System.out.println("ID already exists.");
                    System.out.println("Skipping client "+client);
                    continue;
                }

                PreparedStatement statement = connection
                        .prepareStatement("INSERT into client (id,firstname,lastname,age,moneyspent) VALUES (?,?,?,?,?)");{


                    statement.setInt(1,client.get_ID());
                    statement.setString(2,client.get_first_name());
                    statement.setString(3,client.get_last_name());
                    statement.setInt(4,client.get_age());
                    statement.setFloat(5,client.get_money_spent());

                    statement.executeUpdate();

                    System.out.println("Client "+client+" added to DB succesfully.");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /*
    @Override
    public Optional<Student> delete(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("id must not be null");
        }
        Optional<Student> student = findOne(id); // :(

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM student where id=?")) {
            statement.setLong(1, id);

            statement.executeUpdate();

            return Optional.empty();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return student;
    }

    @Override
    public Optional<Student> update(Student entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("entity must not be null");
        }
        validator.validate(entity);

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection
                     .prepareStatement("UPDATE student SET serial_number=?,name=?,group_number=? where id=?")) {
            statement.setString(1, entity.getSerialNumber());
            statement.setString(2, entity.getName());
            statement.setInt(3, entity.getGroupNumber());
            statement.setLong(4, entity.getId());

            statement.executeUpdate();

            return Optional.empty();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.of(entity);
    }
}
*/



}
