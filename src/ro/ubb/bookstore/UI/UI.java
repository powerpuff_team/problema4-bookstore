package ro.ubb.bookstore.UI;

import org.xml.sax.SAXException;
import ro.ubb.bookstore.controller.Controller;
import ro.ubb.bookstore.model.Book;
import ro.ubb.bookstore.model.Client;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

/**
 * Created by Mapnah on 3/8/2016.
 */
public class UI {
    private Controller ctrl;

    public UI(Controller ctrl){
        this.ctrl = ctrl;
    }

    //BOOK OPERATIONS

    /**
     * The UI book operation controller.This functions takes input from user and calls the right functions based on the input recieved.
     */
    public void book_operations(){
        while(true){
            book_print_menu();

            System.out.println("Choose: ");
            int opt = int_input();

            if(opt==1){add_book();}
            else if(opt==2){update_book();}
            else if(opt==3){remove_book();}
            else if(opt==4){filter_books();}
            else if(opt==5){sort_books();}
            else if(opt==6){print_books();}
            else if(opt==7){restock();}
            else if(opt==8){book_load_XML();}
            else if(opt==9){book_save_XML();}
            else if(opt==10){get_book_from_DB_by_ID();}
            else if (opt==11){get_all_books_from_DB();}
            else if (opt==12){save_books_to_DB();}
            else if(opt==0){break;}
            else{System.out.println("Invalid option!");}
        }
    }




    /**
     * I wonder what this function does
     */

    public void save_books_to_DB(){
        this.ctrl.saveBooksToDB();
    }


    /**
     * This function gets all the books from the database and prints them
     */


    public void get_all_books_from_DB(){
        List<Book> listOfBooks = ctrl.getBooksFromDB();

        for (Book book : listOfBooks){
            System.out.println(book);
        }
    }
    /**
     * This function prints the book returned
     */
    public void get_book_from_DB_by_ID(){
        Long id;
        System.out.println("Give the id of the book.");
        id = long_input();

        Optional<Book> book;
        book = ctrl.getBookByIDFromDB(id);

        if (book.isPresent()){
            System.out.println(book);
        }
        else{
            System.out.println("Book with this ID does not exist.");
        }

    }

    /**
     * Takes from user all the necessary input to create a book and sends it to the right controller function.
     */
    public void add_book(){
        System.out.println("ID: ");
        int id = int_input();

        System.out.println("Title: ");
        String title = string_input();

        System.out.println("Author: ");
        String author = string_input();

        System.out.println("Genre: ");
        String genre = string_input();

        System.out.println("Review: ");
        int review = int_input();

        System.out.println("Price: ");
        float price = float_input();

        System.out.println("Number of copies: ");
        int noOfCopies = int_input();

        ctrl.add_book(id,title,author,genre,review,price,noOfCopies);
    }

    /**
     * Takes from user all the necessary input to update a book and sends it to the right controller function.
     */
    public void update_book(){
        System.out.println("ID: ");
        int id = int_input();

        System.out.println("New title: ");
        String title = string_input();

        System.out.println("New author: ");
        String author = string_input();

        System.out.println("New genre: ");
        String genre = string_input();

        System.out.println("New review: ");
        int review = int_input();

        System.out.println("New price: ");
        float price = float_input();

        System.out.println("New number of copies: ");
        int noOfCopies = int_input();

        ctrl.update_book(id,title,author,genre,review,price,noOfCopies);
    }

    /**
     * Takes from user as input the id of the book that has to be deleted and sends it to the right controller function.
     */
    public void remove_book(){
        System.out.println("ID: ");
        int id = int_input();

        ctrl.remove_book(id);
    }

    /**
     * The UI Book filter controller.Takes input from user and calls the right functions based on the input recieved.
     */
    public void filter_books() {
        while(true){
            book_filter_menu();
            System.out.println("Choose: ");
            int choice = int_input();
            if(choice == 1){
                book_filter_by_title();
            }else if(choice == 2){
                book_filter_by_author();
            }else if(choice == 3){
                book_filter_by_genre();
            }else if(choice == 4){
                book_filter_by_review();
            }else if(choice == 0){
                break;
            }
            else{
                System.out.println("Invalid option!");
            }
        }
    }

    /**
     * Takes title input from user and sends it to the controller.Afterwards,prints everything recieved from controller.
     */

    public void book_filter_by_title() {
        System.out.println("Title:");
        String title = string_input();
        List<Book> result = ctrl.book_filter_by_title(title);
        for(Book b: result){
            System.out.println(b.toString());
        }
    }

    /**
     * Takes author input from user and sends it to the controller.Afterwards,prints everything recieved from controller.
     */
    public void book_filter_by_author() {
        System.out.println("Author:");
        String author = string_input();
        List<Book> result = ctrl.book_filter_by_author(author);
        for(Book b: result){
            System.out.println(b.toString());
        }
    }

    /**
     * Takes genre input from user and sends it to the controller.Afterwards,prints everything recieved from controller.
     */
    public void book_filter_by_genre() {
        System.out.println("Genre:");
        String genre = string_input();
        List<Book> result = ctrl.book_filter_by_genre(genre);
        for(Book b: result){
            System.out.println(b.toString());
        }
    }

    /**
     * Takes review input from user and sends it to the controller.Afterwards,prints everything recieved from controller.
     */

    public void book_filter_by_review() {
        System.out.println("Review:");
        int review = int_input();
        List<Book> result = ctrl.book_filter_by_review(review);
        for(Book b: result){
            System.out.println(b.toString());
        }

    }

    public void sort_books(){
        System.out.println("Sort by price:");
        List<Book> lista = ctrl.book_sort_by_price();
        for(Book b: lista) {
            System.out.println(b);
        }
    }


    /**
     * Takes care of printing the books in the database.
     */
    public void print_books(){
        if(ctrl.get_bookDB_size() == 0){
            System.out.println("Book Database is empty. Nothing to print!");
        }
        else {
            List<Book> bookList = ctrl.get_books();
            for(Book b: bookList){
                System.out.println(b.toString());
            }
        }
    }

    /**
     * Adds to the book with the given ID a number of copies.
     */
    public void restock(){
        System.out.println("Book ID: ");
        int book_id = int_input();

        System.out.println("Number of copies to be added: ");
        int newNoOfCopies = int_input();
        if(newNoOfCopies > 0){
            ctrl.book_restock(book_id,newNoOfCopies);
        }else{
            System.out.println("The number of copies must be positive and at least 1.");
        }
    }


    public void book_load_XML(){
        try {
            ctrl.book_load_XML();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void book_save_XML(){
        ctrl.book_save_XML();
    }

    //CLIENT OPERATIONS

    /**
     * The UI client operation controller.This functions takes input from user and calls the right functions based on the input recieved.
     */
    public void client_operations(){
        while(true){
            client_print_menu();

            System.out.println("Choose: ");
            int opt = int_input();

            if(opt==1){add_client();}
            else if(opt==2){update_client();}
            else if(opt==3){remove_client();}
            else if(opt==4){filter_clients();}
            else if(opt==5){sort_clients();}
            else if(opt==6){print_clients();}
            else if(opt==7){client_buys_book();}
            else if(opt==8){client_load_XML();}
            else if(opt==9){client_save_XML();}
            else if(opt==10){get_client_from_DB_by_ID();}
            else if(opt==11){get_all_clients_from_DB();}
            else if(opt==12){save_clients_to_DB();}
            else if(opt==0){break;}
            else{System.out.println("Invalid option!");}
        }
    }

    /**
     * I wonder what this function does
     */

    public void save_clients_to_DB(){
        this.ctrl.saveClientsToDB();
    }


    /**
     * This function gets all the clients from the database and prints them
     */


    public void get_all_clients_from_DB(){
        List<Client> listOfClients = ctrl.getClientsFromDB();

        for (Client client : listOfClients){
            System.out.println(client);
        }
    }
    /**
     * This function prints the client returned
     */
    public void get_client_from_DB_by_ID(){
        Long id;
        System.out.println("Give the id of the client.");
        id = long_input();

        Optional<Client> client;
        client = ctrl.getClientByIDFromDB(id);

        if (client.isPresent()){
            System.out.println(client);
        }
        else{
            System.out.println("Client with this ID does not exist.");
        }

    }

    /**
     * Takes from user all the necessary input to create a client and sends it to the right function in the controller.
     */
    public void add_client(){
        System.out.println("ID: ");
        int id = int_input();

        System.out.println("First name: ");
        String first_name = string_input();

        System.out.println("Last name: ");
        String last_name = string_input();

        System.out.println("Age: ");
        int age = int_input();

        System.out.println("Money spent: ");
        float money_spent = float_input();

        ctrl.add_client(id,first_name,last_name,age,money_spent);
    }

    /**
     * Takes from user all the necessary input to updateaclient and sends it to the right function in the controller.
     */
    public void update_client(){
        System.out.println("ID: ");
        int id = int_input();

        System.out.println("First name: ");
        String first_name = string_input();

        System.out.println("Last name: ");
        String last_name = string_input();

        System.out.println("Age: ");
        int age = int_input();

        System.out.println("Money spent: ");
        float money_spent = float_input();

        ctrl.update_client(id,first_name,last_name,age,money_spent);
    }

    /**
     * Takes from user the id of the client that has to be removed and sends it to the right function in the controller.
     */
    public void remove_client(){
        System.out.println("ID: ");
        int id = int_input();

        ctrl.remove_client(id);
    }

    /**
     * The UI client filter controller.This functions takes input from user and calls the right functions based on the input recieved.
     */
    public void filter_clients() {
        while (true){
            client_filter_menu();
            System.out.println("Choose:");
            int opt = int_input();
            if(opt == 1){
                client_filter_by_last_name();
            }else if(opt == 2){
                client_filter_by_first_name();
            }else if(opt==3){
                client_filter_by_age();
            }else if(opt==0){
                break;
            }else System.out.println("Invalid option!");
        }
    }

    /**
     * Takes last name input from user and sends it to the controller.Afterwards,prints everything recieved from controller.
     */
    public void client_filter_by_last_name() {
        System.out.println("Last name:");
        String last_name = string_input();
        List<Client> result = ctrl.client_filter_by_last_name(last_name);
        for(Client c: result){
            System.out.println(c.toString());
        }
    }

    /**
     * Takes first name input from user and sends it to the controller.Afterwards,prints everything recieved from controller.
     */
    public void client_filter_by_first_name() {
        System.out.println("First name:");
        String first_name = string_input();
        List<Client> result = ctrl.client_filter_by_first_name(first_name);
        for(Client c: result){
            System.out.println(c.toString());
        }
    }

    /**
     * Takes age input from user and sends it to the controller.Afterwards,prints everything recieved from controller.
     */
    public void client_filter_by_age() {
        System.out.println("Age:");
        int age = int_input();
        List<Client> result = ctrl.client_filter_by_age(age);
        for(Client c: result){
            System.out.println(c.toString());
        }

    }

    public void sort_clients(){
        System.out.println("Sort by age:");
        List<Client> lista = ctrl.client_sort_by_age();
        for(Client c: lista) {
            //System.out.println(c.get_first_name() + " " + c.get_age());
            System.out.println(c);
        }
    }

    /**
     * Takes care of printing the clients.
     */
    public void print_clients(){
        if(ctrl.get_clientDB_size() == 0){
            System.out.println("Client Database is empty. Nothing to print!");
        }
        else {
            List<Client> clientList = ctrl.get_clients();
            for(Client c: clientList){
                System.out.println(c.toString());
            }
        }
    }

    /**
     * Takes the ID of the book to be sold and that of the client that buys and also the number of copies desired tobe bought and sends them to the rght function in the controller.
     */
    public void client_buys_book() {
        System.out.println("Book ID: ");
        int book_id = int_input();

        System.out.println("Client ID: ");
        int client_id = int_input();

        System.out.println("Number of copies to be bought: ");
        int copiesToBeBought = int_input();

        if (copiesToBeBought <= 0) {
            System.out.println("Client cannot buy 0 or less copies!");
        } else {
            ctrl.client_buys_book(client_id, book_id, copiesToBeBought);
        }
    }

    public void client_load_XML(){
        try {
            ctrl.client_load_XML();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void client_save_XML(){
        ctrl.client_save_XML();
    }

    //MAIN MENU

    /**
     * The starting function of the UI.Takes input from user and based on that input calls the right functions.
     */
    public void run(){
        while(true) {
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println("         Main Menu        ");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println("1. Book Menu");
            System.out.println("2. Client Menu");
            System.out.println("0. Exit");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");

            System.out.println("Choose: ");
            int opt = int_input();
            if (opt == 1){
                book_operations();
            }
            else if(opt == 2){
                client_operations();
            }
            else if (opt == 0){
                break;
            }
            else{
                System.out.println("Invalid option!");
            }
        }
    }

    /**
     * Book menu
     */
    public void book_print_menu(){
        System.out.println("~~~~~~~~~~~~~~~");
        System.out.println("1.Add");
        System.out.println("2.Update");
        System.out.println("3.Remove");
        System.out.println("4.Filter");
        System.out.println("5.Sort");
        System.out.println("6.Print");
        System.out.println("7.Restock");
        System.out.println("8. Load from XML file.");
        System.out.println("9. Write to XML file.");
        System.out.println("10. Get book by ID from Database.");
        System.out.println("11. Get all books from Database.");
        System.out.println("12. Save books to Database");
        System.out.println("0.Exit");
    }

    /**
     * Client menu
     */
    public void client_print_menu(){
        System.out.println("~~~~~~~~~~~~~~~");
        System.out.println("1.Add");
        System.out.println("2.Update");
        System.out.println("3.Remove");
        System.out.println("4.Filter");
        System.out.println("5.Sort");
        System.out.println("6.Print");
        System.out.println("7.Buy book");
        System.out.println("8. Load from XML file.");
        System.out.println("9. Write to XML file.");
        System.out.println("10. Get client by ID from Database.");
        System.out.println("11. Get all clients from Database.");
        System.out.println("12. Save clients to Database");
        System.out.println("0.Exit");
    }

    /**
     * Book filter menu
     */
    public void book_filter_menu(){
        System.out.println("~~~~~~~~~~~~~~~");
        System.out.println("Sort by:");
        System.out.println("1.Title");
        System.out.println("2.Author");
        System.out.println("3.Genre");
        System.out.println("4.Review");
        System.out.println("0.Exit");
    }

    /**
     * Client filter menu
     */
    public void client_filter_menu(){
        System.out.println("~~~~~~~~~~~~~~~");
        System.out.println("Sort by:");
        System.out.println("1.Last name");
        System.out.println("2.First name");
        System.out.println("3.Age");
        System.out.println("0.Exit");

    }

    /**
     * Takes int input from user.
     * @return Int input recieved from user.
     */
    public int int_input(){
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }


    public long long_input(){
        Scanner sc = new Scanner(System.in);
        return sc.nextLong();
    }

    /**
     * Takes string input from user.
     * @return String input recieved from user.
     */
    public String string_input(){
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    /**
     * Takes float input from user.
     * @return Float input recieved from user.
     */
    public float float_input(){
        Scanner sc = new Scanner(System.in);
        return sc.nextFloat();
    }

}
