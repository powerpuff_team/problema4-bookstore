package ro.ubb.bookstore.model;

/**
 * Created by Dani on 3/7/2016.
 */
public class Book {
    private int id;
    private String title;
    private String author;
    private String genre;
    private int review; //note de la 0 la 5 (aka stelute)
    private float price;
    private int noOfCopies;

    public Book(int id,String title,String author,String genre,int review,float price,int noOfCopies){
        this.id=id;
        this.title=title;
        this.author=author;
        this.genre=genre;
        this.review=review;
        this.price=price;
        this.noOfCopies=noOfCopies;
    }

    //GETTERS
    public int get_ID(){
        return id;
    }
    public String get_title(){
        return title;
    }
    public String get_author(){
        return author;
    }
    public String get_genre(){
        return genre;
    }
    public int get_review(){
        return review;
    }
    public float get_price(){
        return price;
    }
    public int get_noOfCopies(){
        return noOfCopies;
    }

    //SETTERS
    public void set_title(String new_title){
        title=new_title;
    }
    public void set_author(String new_author){
        author=new_author;
    }
    public void set_genre(String new_genre){
        genre=new_genre;
    }
    public void set_review(int new_review){
        review=new_review;
    }
    public void set_price(float new_price){
        price=new_price;
    }
    public void set_noOfCopies(int new_noOfCopies){
        noOfCopies=new_noOfCopies;
    }

    /**
     *
     * @return Review in stars format(e.g. if review=3, result will be "***")
     */
    private String reviewToString(){
        if(review == 0)
            return "0";
        String result = "";
        for(int i=0;i<review;i++){
            result+="*";
        }
        return result;
    }

    /**
     *
     * @return An A E S T H E T I C string,representing the book
     */
    @Override
    public String toString(){
        return "ID:"+id+"; Title:"+title+"; Author:"+author+"; Genre:"+genre+"; Review:"+reviewToString()+"; Price:"+price+"; Number of copies:"+noOfCopies;
    }

}
