package ro.ubb.bookstore.model.validators;

import ro.ubb.bookstore.model.Book;

/**
 * Created by Dani on 3/7/2016.
 */
public class BookValidator {

    /**
     * Checks whether the book's fields are valid or not.
     * @param book
     * @return A string containing all the errors with the book's fields.
     */
    public String validate(Book book){
        String errors="";
        if(book.get_ID() <= 0){
            errors+="Book's ID cannot be equal or less than 0;\n";
        }
        if(book.get_title().equals("")){
            errors+="Book's title cannot be empty;\n";
        }
        if(book.get_author().equals("")){
            errors+="Book's author name must be given;\n";
        }
        if(book.get_genre().equals("")){
            errors+="Book's genre must be given;\n";
        }
        if(book.get_review() < 0 || book.get_review() > 5){
            errors+="Book's review must be between 0 and 5;\n";
        }
        if(book.get_price() < 0){
            errors+="Book's price mut be a positive number;\n";
        }
        if(book.get_noOfCopies() < 0) {
            errors += "Book's number of copies cannot be negative;\n";
        }
        return errors;
    }
}
