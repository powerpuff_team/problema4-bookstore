package ro.ubb.bookstore.model.validators;

import ro.ubb.bookstore.model.Client;

/**
 * Created by Dani on 3/7/2016.
 */
public class ClientValidator {

    /**
     * Checks whether the client's fields are valid or not.
     * @param client
     * @return A string containing all the errors with the client's fields.
     */
    public String validate(Client client){
        String errors="";
        if (client.get_ID() <= 0){
            errors+= "Client's ID cannot be equal or less than 0;\n";
        }
        if (client.get_last_name().equals("")){
            errors += "Client's last name cannot be empty;\n";
        }
        if(client.get_first_name().equals("")){
            errors+= "Client's first name cannot be empty;\n";
        }
        if(client.get_age() <= 13) {
            errors += "Client's age cannot be less than 14;\n";
        }
        return errors;
    }
}
