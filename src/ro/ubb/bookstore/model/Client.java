package ro.ubb.bookstore.model;

/**
 * Created by Mapnah on 3/7/2016.
 */
public class Client{

    private int id;
    private String first_name;
    private String last_name;
    private int age;
    private float money_spent;

    public Client(int id,String first_name,String last_name,int age,float money_spent){
        this.id=id;
        this.first_name=first_name;
        this.last_name=last_name;
        this.age=age;
        this.money_spent=money_spent;
    }

    //GETTERS
    public int get_ID(){
        return id;
    }
    public String get_first_name(){
        return first_name;
    }
    public String get_last_name(){
        return last_name;
    }
    public int get_age(){
        return age;
    }
    public float get_money_spent(){
        return money_spent;
    }

    //SETTERS
    public void set_first_name(String new_first_name){
        first_name=new_first_name;
    }
    public void set_last_name(String new_last_name){
        last_name=new_last_name;
    }
    public void set_age(int new_age){
        age=new_age;
    }
    public void set_money_spent(float new_amount){
        money_spent=new_amount;
    }


    /**
     *
     * @return An aesthetic string representing the client.
     */
    @Override
    public String toString(){
        return "ID:"+id+"; Name:"+last_name+" "+first_name+"; Age:"+age+"; Money spent:"+money_spent;
    }

//    @Override
//    public int compareTo(Client c) {
//        int result = 0;
//        if(this.get_age() == c.get_age()) result = 0;
//        if(this.get_age() < c.get_age()) result = 1;
//        if(this.get_age() > c.get_age()) result = -1;
//
//        return result;
//    }
}
